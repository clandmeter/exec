#!/bin/sh

#shellcheck disable=SC3040
set -eu -o pipefail

export DOCKER_BUILDKIT=1

msg() {
    echo "-> " "$@"
}

fatal() {
    echo "FATAL:" "$@" >&2
    exit 1
}

docker_login() {
    docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
}

arch_to_docker() {
   case $1 in
        x86_64) echo linux/amd64;;
        x86) echo linux/i386;;
        aarch64) echo linux/arm64/v8;;
        armv7) echo linux/arm/v7;;
        armhf) echo linux/arm/v6;;
        ppc64le) echo linux/ppc64le;;
        s390x) echo linux/s390x;;
        riscv64) echo linux/riscv64;;
    esac
}

cmd_from_env() {
    msg "Logging in to docker"
    docker_login

    msg "Executing command $EXEC_COMMAND"
    case $EXEC_COMMAND in
        build) command_build;;
        publish) command_publish;;
        manifest) command_manifest;;
    esac
}

build_args_from_env() {
    for arg_var in $(env | awk -F= '/^ARG_/ {print $1}'); do
        value=$(eval "echo \$$arg_var")
        varname=${arg_var#ARG_*}
        printf -- "%s=%s\n" "$varname" "$value"
    done
}

ci_tag() {
    if [ -n "${ARCH-}" ]; then
        echo "ci-$CI_COMMIT_SHORT_SHA-$ARCH"
    else
        echo "ci-$CI_COMMIT_SHORT_SHA"
    fi
}

ci_image() {
    image="$CI_REGISTRY_IMAGE"
    if [ -n "$COMPONENT" ]; then
        image="$image/$COMPONENT"
    fi

    echo "$image:$(ci_tag)"
}

publish_tag() {
    tag=$1
    arch=$2
    if [ -n "${arch-}" ]; then
        echo "$tag-$arch"
    else
        echo "$tag"
    fi
}

publish_image() {
    base_tag=$1
    arch=$2

    image="$CI_REGISTRY_IMAGE"
    if [ -n "$COMPONENT" ]; then
        image="$image/$COMPONENT"
    fi
    echo "$image:$(publish_tag "$base_tag" "$arch")"
}

docker_tag() {
    tag=${DOCKER_TAG-latest}
    if [ -n "${CI_COMMIT_TAG-}" ]; then
        tag="$CI_COMMIT_TAG"
    fi
    tag=${tag#"$COMPONENT-"}
    echo "$tag"
}

command_build() {
    set -- -t "$(ci_image)"
    while read -r build_arg; do
        if [ -n "$build_arg" ]; then
            set -- "$@" --build-arg "$build_arg"
        fi
    done <<-EOF
		$(build_args_from_env)
	EOF
    set -- "$@" "${SUBDIR:-.}"
    docker build "$@"
    if [ -z "${LOCAL-}" ]; then
    docker push "$(ci_image)"
    fi
}

command_publish() {
    : "${DOCKER_TAG:=latest}"

    publish_image=$(publish_image "$(docker_tag)" "${ARCH-}")
    msg "Publishing image as $publish_image"

    if [ -z "${LOCAL-}" ]; then
        docker pull "$(ci_image)"
    fi
    docker tag "$(ci_image)" "$publish_image"
    if [ -z "${LOCAL-}" ]; then
        docker push "$publish_image"
    fi
}

command_manifest() {
    if [ -z "${MANIFEST_ARCHES-}" ]; then
        fatal "DOCKER_MANIFEST_ARCHES not provided"
    fi

    images=
    for arch in $MANIFEST_ARCHES; do
        arch_image=$(publish_image "$(docker_tag)" "$arch")
        if [ -z "${LOCAL-}" ]; then
            docker pull "$arch_image"
        fi
        images="$images $arch_image"
    done

    manifest_image=$(publish_image "$(docker_tag)" "")
    #shellcheck disable=SC2086
    docker manifest create --amend "$manifest_image" $images
    docker manifest push "$manifest_image"
}

if [ -n "${CI-}" ]; then
    cd "$CI_PROJECT_DIR"
fi

: "${COMPONENT:=${EXEC_PROJECT-}}" # Backwards compattibility
: "${SUBDIR:=${COMPONENT-}}"

if [ -n "$SUBDIR" ] && ! [ -d "$SUBDIR" ]; then
    fatal "directory $SUBDIR does not exist"
fi

if [ -n "${ARCH-}" ]; then
    DOCKER_DEFAULT_PLATFORM="$(arch_to_docker "$ARCH")"
    export DOCKER_DEFAULT_PLATFORM
fi

case "${1-}" in
    bash|sh)
        if [ -z "${CI-}" ]; then
            shift
            exec sh "$@"
        else
            cmd_from_env
        fi
        ;;
    build) command_build;;
    publish) command_publish;;
    manifest) command_manifest;;
    *) echo "Unknown command: ${1-}";;
esac
